function editNav() {
    var x = document.getElementById("myTopnav")
    if (x.className === "topnav") {
        x.className += " responsive"
    } else {
        x.className = "topnav"
    }
  }
  
// DOM Elements
const modalbg = document.querySelector(".bground");
const formData = document.querySelectorAll(".formData");

// launch modal form
const modalBtn = document.querySelectorAll(".modal-btn")
modalBtn.forEach((btn) => btn.addEventListener("click", launchModal))
function launchModal() {
    modalbg.style.display = "block";
  }
  
// launch Thx-message
const thxMessage = document.querySelector(".thx-message")
  function launchMessage() {
    thxMessage.style.display = "block";
  }

// close span for Modal form and Thx-Message
const closeBtn = document.querySelectorAll(".close");
closeBtn.forEach((close) => close.addEventListener("click", closeModal));
function closeModal() {
    modalbg.style.display = "none";
    thxMessage.style.display = "none";
  }

// close btn for Thx-Message
const closeThxBtn = document.querySelector(".btn-close");
closeThxBtn.addEventListener("click", closeThxMessage);
function closeThxMessage() {
    thxMessage.style.display = "none";
  }

// Select labels and error msg
const first = document.getElementById("first");
const firstMsg = document.querySelector(".first-msg");
const last = document.getElementById("last");
const lastMsg = document.querySelector(".last-msg");
const email = document.getElementById("email");
const emailMsg = document.querySelector(".email-msg");
const birthdate = document.getElementById("birthdate");
const birthdateMsg = document.querySelector(".birthdate-msg");
const quantity = document.getElementById("quantity");
const quantityMsg = document.querySelector(".quantity-msg");
const checkbox = document.getElementById("checkbox1");
const checkboxMsg = document.querySelector(".checkbox-msg");
const locations = document.querySelectorAll("input[name='location']");
const locationMsg = document.querySelector(".location-msg");

// check if first is valid
function validFirstName(first) {
    return /^(?=.{2,30}$)[A-Za-z]+(?:['_.\s][a-z]+)*$/.test(first);
};

// check if last is valid
function validLastName(last) {
    return /^(?=.{2,30}$)[A-Za-z]+(?:['_.\s][a-z]+)*$/.test(last);
};

// check if email is valid
function validEmail(email) {
    return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email);
};

// check if birthdate is valid
function validBirthdate(birthdate) {
    return /^[1-2]\d{3}-(((0)[0-9])|((1)[0-2]))-([0-2][0-9]|(3)[0-1])$/.test(birthdate);
};

// check if at least one location of locations is checked 
function checkLocations() {
    let locationChecked = 0;
    for (const location of locations) {
        if (location.checked) {
            locationChecked++;
        }
        if (locationChecked > 0) return true;
    }
    return false;
  }
  
// select btn-submit and test every conditions
  
const submitBtn = document.getElementById("btn-submit")
submitBtn.addEventListener("click", function (e) {
    e.preventDefault();
  
    let error = 0;
  
    if (!validFirstName(first.value)) {
            firstMsg.innerHTML ="Veuillez entrer 2 caractères ou plus pour le champ du prénom.";
            error++;
        } else {
            firstMsg.innerHTML ="";
        };
  
    if (!validLastName(last.value)) {
            lastMsg.innerHTML ="Veuillez entrer 2 caractères ou plus pour le champ du nom.";
            error++;
        } else {
            lastMsg.innerHTML ="";
        };
  
    if (!validEmail(email.value)) {
            emailMsg.innerHTML = "Vous devez entrer un email valide.";
            error++;
         } else {
            emailMsg.innerHTML ="";
        };
  
    if (!validBirthdate(birthdate.value)) {
            birthdateMsg.innerHTML = "Vous devez entrer votre date de naissance.";
            error++;
        } else {
            birthdateMsg.innerHTML ="";
        };
  
    if (quantity.value === "" || quantity.value < 0) {
            quantityMsg.innerHTML = "Vous devez saisir une valeur numérique";
            error++;
        } else {
            quantityMsg.innerHTML ="";
        };
  
    if (!checkLocations()) {
            locationMsg.innerHTML = "Vous devez choisir une option.";
            error++;
        } else {
            locationMsg.innerHTML ="";
        };
  
    if (!checkbox.checked) {
            checkboxMsg.innerHTML = "Vous devez vérifier que vous acceptez les termes et conditions.";
            error++;
        } else {
            checkboxMsg.innerHTML ="";
        };
//   if error = 0 => close Modal Form, open Thx-Message and reset inputs values
    if (error === 0 ) {
            closeModal();
            launchMessage();
            document.getElementById("form").reset();
        };
    });